#include "SourceFile.h"

SourceFile::SourceFile(std::string fileName, std::string contents) {
	this->FileName.assign(fileName);

	this->Contents.assign(contents);

	auto rawContents = contents.c_str();
	auto rawContentsLength = strlen(rawContents);
	std::string line;

	for(size_t x = 0; x < rawContentsLength; x++) {
		if(rawContents[x] == '\n') {
			this->Lines.push_back(line);
			line.clear();
			continue;
		}

		line.insert(line.end(), rawContents[x]);
	}

	this->Lines.push_back(line);
}

SourceFile::~SourceFile() {

}

std::string SourceFile::GetContents() {
	return this->Contents;
}

std::string SourceFile::GetFileName() {
	return this->FileName;
}

SourceFile *SourceFile::Open(std::string fileName) {
	std::ifstream file(fileName);

	if(!file.good()) {
		file.close();
		return NULL;
	}

	std::string contents, line;
	auto firstLine = true;

	while(std::getline(file, line)) {
		if(!firstLine) {
			contents.append("\n");
		}
		else {
			firstLine = false;
		}

		contents.append(line);
	}

	file.close();
	return new SourceFile(fileName, contents);
}

std::pair<unsigned int,unsigned int> SourceFile::Lookup(unsigned int index) {
	size_t lineIndex = 0;
	for(size_t l = 0; l < this->Lines.size(); l++) {
		auto lineLength = this->Lines.at(l).size();

		if(index >= lineIndex && index <= (lineIndex + lineLength)) {
			return std::pair<int, int>(l + 1, index - lineIndex + 1);
		}

		lineIndex += lineLength + 1;
	}

	return std::pair<int, int>(0, 0);
}