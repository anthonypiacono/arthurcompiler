#ifndef __REGEXPP_H__
#define __REGEXPP_H__

#include <string>
#include <regex>
#include <vector>
#include <iostream>

typedef std::string (*RegexPPSimpleReplaceCallback)(std::string match);

class RegexPP {
private:
	RegexPP();

public:
	static size_t SimpleReplaceCallback(std::string pattern, std::string haystack, std::string &output, RegexPPSimpleReplaceCallback callback);
	static std::vector<std::pair<std::string, unsigned int>> MatchAll(std::string pattern, std::string haystack);
};

#endif