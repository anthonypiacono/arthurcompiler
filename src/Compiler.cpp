#include "Compiler.h"

Compiler g_Compiler;

Compiler::Compiler() {
}

Compiler::~Compiler() {
}

bool Compiler::Compile(std::string inputFile, std::string outputFile, std::vector<std::string> includeDirectories, std::vector<std::string> *errors) {
	inputFile = full_path(inputFile);
	Compilation compilation(inputFile);

	std::map<unsigned int, boost::function<void (SourceTree*, Token*, unsigned int)>> stateCallbacks;
	stateCallbacks.insert(std::pair<unsigned int, boost::function<void (SourceTree*, Token*, unsigned int)>>(STATE_DEFAULT, boost::bind(&Compilation::Default, &compilation, _1, _2, _3)));
	stateCallbacks.insert(std::pair<unsigned int, boost::function<void (SourceTree*, Token*, unsigned int)>>(STATE_INCLUDE, boost::bind(&Compilation::Include, &compilation, _1, _2, _3)));

	auto tree = g_Lexer.Lex(inputFile);

	if(tree == NULL) {
		errors->push_back("Error in syntax in file: " + inputFile);
		return false;
	}

	auto size = tree->Size();

	for(unsigned int x = 0; x < tree->Size(); x++) {
		auto t = tree->At(x);

		auto state = compilation.GetState();

		stateCallbacks[state](tree, t, x);
		
		if(compilation.GetLastError()) {
			errors->push_back("TODO: ERROR"); // TODO
		}
	}

	if(errors->size()) {
		return false;
	}

	return true;
}