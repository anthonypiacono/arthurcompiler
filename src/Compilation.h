#pragma once

#include "SourceTree.h"
#include "Token.h"
#include "Lexer.h"
#include <boost/filesystem.hpp>

enum CompilationState {
	STATE_DEFAULT = 0,
	STATE_INCLUDE
};

enum CompilationError {
	ERROR_NONE = 0,
	ERROR_TOKEN_NOT_ALLOWED,
	ERROR_EXPECTED_STRING,
	ERROR_INCLUDE,
	ERROR_COUNT
};

class Compilation {
public:
	Compilation(std::string inputFile);
	~Compilation();

	unsigned int GetState();
	unsigned int GetLastError() { return LastError; }

	void Error(unsigned int error);
	void IncludedFile(std::string inputFile);
	bool FileAlreadyIncluded(std::string inputFile);

	void Default(SourceTree *tree, Token *t, unsigned int index);
	void Include(SourceTree *tree, Token *t, unsigned int index);

private:
	unsigned int State;
	unsigned int LastError;
	std::vector<std::string> IncludedFiles;
};

std::string full_path(std::string inputFile);