#include "Compilation.h"

Compilation::Compilation(std::string inputFile) {
	State = 0;
	LastError = 0;
	IncludedFile(full_path(inputFile));
}

Compilation::~Compilation() {
}

unsigned int Compilation::GetState() {
	return State;
}

void Compilation::Error(unsigned int error) {
	LastError = error;
}

void Compilation::IncludedFile(std::string inputFile) {
	IncludedFiles.push_back(inputFile);
}

bool Compilation::FileAlreadyIncluded(std::string inputFile) {
	auto size = IncludedFiles.size();
	for(unsigned int x = 0; x < size; x++) {
		if(inputFile == IncludedFiles.at(x)) {
			return true;
		}
	}

	return false;
}

void Compilation::Default(SourceTree *tree, Token *t, unsigned int index) {
	auto operation = t->Op();

	if(operation == "PREPROCESSOR_INCLUDE") {
		State = STATE_INCLUDE;
		return;
	}

	Error(ERROR_TOKEN_NOT_ALLOWED);
}

std::string full_path(std::string inputFile) {
	boost::filesystem::path p(inputFile);

	if(p.is_relative()) {
		p = boost::filesystem::path(boost::filesystem::current_path()) /= p;
	}

	p = p.make_preferred();

    boost::filesystem::path result;

    for(boost::filesystem::path::iterator it=p.begin(); it!=p.end(); ++it) {
        if(*it == "..") {
            if(boost::filesystem::is_symlink(result)) {
                result /= *it;
			}
            else if(result.filename() == "..") {
                result /= *it;
			}
            else {
                result = result.parent_path();
			}
        }
        else if(*it != ".") {
			result /= *it;
		}
    }

    return result.make_preferred().string();
}

void Compilation::Include(SourceTree *tree, Token *t, unsigned int index) {
	auto operation = t->Op();

	if(operation == "WHITESPACE") {
		return;
	}

	if(operation != "STRING") {
		Error(ERROR_EXPECTED_STRING);
		return;
	}

	auto path = full_path(t->Param(0));

	if(FileAlreadyIncluded(path)) {
		std::cout << "WARNING: Included file already, skipping.";
		return;
	}

	auto includeTree = g_Lexer.Lex(path);

	IncludedFile(path);

	if(includeTree == NULL) {
		Error(ERROR_INCLUDE);
		return;
	}

	tree->Insert(includeTree, index + 1);

	delete includeTree;
}