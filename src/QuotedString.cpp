#include "QuotedString.h"
#include "RegexPP.h"

char CharToBase16(char c) {
	if(c >= 'a') {
		c -= 'a';
		c += 10;
	} else if(c >= '0') {
		c -= '0';
	}
	return c;
}

std::string ReplaceEscapeCharacter(std::string match) {
	auto escapeChar = match.at(1);

	if(escapeChar == 'x') {
		std::string hexValueStr = boost::to_lower_copy(match.substr(2, 2));

		auto msbChar = CharToBase16(hexValueStr.at(0));
		auto lsbChar = CharToBase16(hexValueStr.at(1));

		unsigned char replaceWith = lsbChar + (msbChar * 16);
		return std::string(1, replaceWith);
	}

	class replacement {
	public:
		replacement(std::string r, char rW) {
			this->replace = r;
			this->replaceWith = rW;
		}

		std::string replace;
		char replaceWith;
	};

	std::vector<replacement> replacements;

	replacements.push_back(replacement("a", '\a'));
	replacements.push_back(replacement("b", '\b'));
	replacements.push_back(replacement("f", '\f'));
	replacements.push_back(replacement("n", '\n'));
	replacements.push_back(replacement("r", '\r'));
	replacements.push_back(replacement("t", '\t'));
	replacements.push_back(replacement("\\", '\\'));
	replacements.push_back(replacement("'", '\''));
	replacements.push_back(replacement("\"", '"'));

	auto replacementsCount = replacements.size();
	auto escapeCharStr = std::string(1, escapeChar);

	for(size_t x = 0; x < replacementsCount; x++) {
		auto r = replacements.at(x);
		
		if(r.replace != escapeCharStr) {
			continue;
		}
		
		return std::string(1, r.replaceWith);
	}

	return std::string();
}

QuotedString::QuotedString(std::string quotedStr) {
	this->DoubleQuoted = quotedStr.at(0) != '"';

	std::string output(quotedStr.substr(1, quotedStr.size() - 2));

	auto replacementCount = RegexPP::SimpleReplaceCallback("\\\\((x[0-9a-fA-F][0-9a-fA-F])|(.))", output, output, ReplaceEscapeCharacter);

	this->String.assign(output);
}

bool QuotedString::IsDoubleQuoted() {
	return this->DoubleQuoted;
}

std::string QuotedString::Str() {
	return this->String;
}