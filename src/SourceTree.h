#ifndef __SOURCETREE_H__
#define __SOURCETREE_H__

#include <vector>
#include "Token.h"

class SourceTree {
public:
	SourceTree();
	~SourceTree();

	void Append(Token *token);
	void Prepend(Token *token);
	Token *At(size_t index);
	size_t Size();
	void Insert(Token *token, unsigned int index);
	void Insert(SourceTree *tree, unsigned int index);

private:
	std::vector<Token*> Tokens;
};

#endif