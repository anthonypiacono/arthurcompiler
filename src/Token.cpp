#include "Token.h"

Token::Token(std::string operation, std::vector<std::string> parameters) {
	this->Operation = operation;
	this->Parameters = parameters;
}

Token::Token(std::string operation) {
	this->Operation = operation;
}

std::string Token::Op() {
	return this->Operation;
}

size_t Token::ParamCount() {
	return this->Parameters.size();
}

std::string Token::Param(size_t n) {
	if(n >= this->ParamCount()) {
		return "";
	}

	return this->Parameters.at(n);
}

std::vector<std::string> Token::Params() {
	return this->Parameters;
}

void Token::Tag(std::string fileName, unsigned int line, unsigned int character, std::string text, unsigned int length) {
	FileName.assign(fileName);
	Line = line;
	Character = character;
	Text.assign(text);
	Length = length;
}