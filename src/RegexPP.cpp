#include "RegexPP.h"

size_t RegexPP::SimpleReplaceCallback(std::string pattern, const std::string haystack, std::string &output, RegexPPSimpleReplaceCallback callback) {
	std::regex p(pattern);

	std::string haystackCopy(haystack);

	std::sregex_iterator start(haystack.begin(), haystack.end(), p), end;

	int charactersAdded = 0;
	size_t replacements = 0;

	for(; start != end; start++) {
		auto match = *start; // dereference the iterator to get the match_result

		auto matchStr = match.str(); // store copy of the match's string
		
		auto matchPosition = match.position() + charactersAdded; // offset the match's position by the number of character's added so far in the replace sequence

		auto replaceWith = callback(matchStr); // replace this match with the return value of the callback
		
		haystackCopy.assign(
			haystackCopy.substr(0, matchPosition)
			+ replaceWith
			+ haystackCopy.substr(matchPosition + matchStr.length())
		);

		charactersAdded += replaceWith.length() - matchStr.length();

		replacements++;
	}

	output.assign(haystackCopy);

	return replacements;
}

std::vector<std::pair<std::string, unsigned int>> RegexPP::MatchAll(std::string pattern, std::string haystack) {
	std::vector<std::pair<std::string, unsigned int>> matches;

	std::regex p(pattern);

	std::sregex_iterator start(haystack.begin(), haystack.end(), p), end;

	for(; start != end; start++) {
		auto match = *start; // dereference the iterator to get the match_result
		matches.push_back(std::pair<std::string, unsigned int>(match.str(), match.position()));
	}

	return matches;
}