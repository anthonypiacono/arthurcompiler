#ifndef __LEXER_H__
#define __LEXER_H__

#include <string>
#include <map>
#include <regex>
#include <boost/algorithm/string.hpp>
#include "SourceTree.h"
#include "SourceFile.h"
#include "QuotedString.h"
#include "RegexPP.h"

typedef Token* (*TokenPatternCallback)(std::string str);

struct TokenPattern {
	std::string pattern;
	TokenPatternCallback callback;
};

class Lexer {
public:
	Lexer();

	SourceTree *Lex(std::string fileName);
	void AddTokenPattern(std::string pattern, TokenPatternCallback callback);

private:
	std::vector<TokenPattern*> TokenPatterns;
};

extern Lexer g_Lexer;

#endif