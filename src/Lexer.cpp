// Lexer.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include "Lexer.h"
#include <regex>

Lexer g_Lexer;

Token *TokenPatternCallback_Whitespace(std::string str) {
	return new Token("WHITESPACE");
}

Token *TokenPatternCallback_Comment(std::string str) {
	std::vector<std::string> params;
	params.push_back(str);
	return new Token("COMMENT", params);
}

Token *TokenPatternCallback_Preprocessor(std::string str) {
	return new Token("PREPROCESSOR_" + boost::to_upper_copy(str.substr(1)));
}

Token *TokenPatternCallback_JSHeredoc(std::string str) {
	std::vector<std::string> params;
	params.push_back(str);
	return new Token("HEREDOC_JS", params);
}

Token *TokenPatternCallback_SQLHeredoc(std::string str) {
	std::vector<std::string> params;
	params.push_back(str);
	return new Token("HEREDOC_SQL", params);
}

Token *TokenPatternCallback_HTMLHeredoc(std::string str) {
	std::vector<std::string> params;
	params.push_back(str);
	return new Token("HEREDOC_HTML", params);
}
Token *TokenPatternCallback_Semicolon(std::string str) {
	return new Token("SEMICOLON");
}

Token *TokenPatternCallback_Symbol(std::string str) {
	if(str == "var") {
		return new Token("KEYWORD_VAR");
	}

	std::vector<std::string> params;
	params.push_back(str);
	return new Token("SYMBOL", params);
}

Token *TokenPatternCallback_Operator(std::string str) {
	return new Token("OPERATOR_" + str);
}

Token *TokenPatternCallback_Dot(std::string str) {
	return new Token("DOT");
}

Token *TokenPatternCallback_Parathensis(std::string str) {
	return new Token(std::string("PARATHENSIS_") + std::string(str == "(" ? "BEGIN" : "END"));
}

Token *TokenPatternCallback_BracketCurly(std::string str) {
	return new Token(std::string("BRACKET_CURLY_") + std::string(str == "{" ? "BEGIN" : "END"));
}

Token *TokenPatternCallback_BracketSquare(std::string str) {
	return new Token(std::string("BRACKET_SQUARE_") + std::string(str == "[" ? "BEGIN" : "END"));
}

Token *TokenPatternCallback_RegexPattern(std::string str) {
	std::vector<std::string> params;
	str.assign(str.substr(1, str.size() - 2));

	params.push_back(str);

	return new Token("REGEX_PATTERN", params);
}

Token *TokenPatternCallback_Comma(std::string str) {
	return new Token("COMMA");
}

Token *TokenPatternCallback_Colon(std::string str) {
	return new Token("COLON");
}

Token *TokenPatternCallback_Tilde(std::string str) {
	return new Token("TILDE");
}

Token *TokenPatternCallback_Number(std::string str) {
	std::vector<std::string> params;
	params.push_back(str);
	return new Token("NUMBER", params);
}

Token *TokenPatternCallback_DoubleQuotedString(std::string str) {
	std::vector<std::string> params;
	params.push_back(QuotedString(str).Str());

	return new Token("STRING", params);
}

Token *TokenPatternCallback_SingleQuotedString(std::string str) {
	std::vector<std::string> params;
	params.push_back(QuotedString(str).Str());

	return new Token("STRING", params);
}

Lexer::Lexer() {
	this->AddTokenPattern("[\\n\\s]+", TokenPatternCallback_Whitespace);
	this->AddTokenPattern("\\/\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*(\\*)+/", TokenPatternCallback_Comment);
	this->AddTokenPattern("\\#(include)", TokenPatternCallback_Preprocessor);
	this->AddTokenPattern("\\;", TokenPatternCallback_Semicolon);
	this->AddTokenPattern("-?(0|([1-9][0-9]*))(\\.[0-9]+)?([eE]-?(0|([1-9][0-9]*)))?", TokenPatternCallback_Number);
	this->AddTokenPattern("\"(?:[^\\\\\"]|\\\\.)*\\\"", TokenPatternCallback_DoubleQuotedString);
	this->AddTokenPattern("\'(?:[^\\\\\']|\\\\.)*\\\'", TokenPatternCallback_SingleQuotedString);
	this->AddTokenPattern("<<<JS\\n(.|\\n)+?\\nJS", TokenPatternCallback_JSHeredoc);
	this->AddTokenPattern("<<<SQL\\n(.|\\n)+?\\nSQL", TokenPatternCallback_SQLHeredoc);
	this->AddTokenPattern("<<<HTML\\n(.|\\n)+?\\nHTML", TokenPatternCallback_HTMLHeredoc);
	this->AddTokenPattern("\\/(?:[^\\\\/]|\\\\.)*\\/", TokenPatternCallback_RegexPattern);
	this->AddTokenPattern("[a-zA-Z_]([a-zA-Z0-9_]+)?", TokenPatternCallback_Symbol);
	this->AddTokenPattern("(>=)|(<=)|>|<|(==)|=|(\\+\\+)|\\+|-|(--)", TokenPatternCallback_Operator);
	this->AddTokenPattern("\\(|\\)", TokenPatternCallback_Parathensis);
	this->AddTokenPattern("\\[|\\]", TokenPatternCallback_BracketSquare);
	this->AddTokenPattern("\\{|\\}", TokenPatternCallback_BracketCurly);
	this->AddTokenPattern("\\.", TokenPatternCallback_Dot);
	this->AddTokenPattern("\\,", TokenPatternCallback_Comma);
	this->AddTokenPattern("\\:", TokenPatternCallback_Colon);
	this->AddTokenPattern("~", TokenPatternCallback_Tilde);
}

SourceTree *Lexer::Lex(std::string fileName) {
	auto tree = new SourceTree();

	auto sourceFile = SourceFile::Open(fileName);
	if(!sourceFile) {
		return NULL;
	}

	auto fileContents = sourceFile->GetContents();
	auto fileContentsStillPendingLexing = fileContents;
	size_t lexedBytes = 0;

	auto fileContentsLength = fileContents.size();

	while(lexedBytes != fileContentsLength) {
		auto foundToken = false;

		for(size_t x = 0; x < this->TokenPatterns.size(); x++) {
			auto tokenPattern = this->TokenPatterns.at(x);
		
			std::regex p(tokenPattern->pattern, std::tr1::regex_constants::ECMAScript);

			std::cmatch matches;

			if(!std::regex_search(fileContentsStillPendingLexing.c_str(), matches, p)) {
				continue;
			}

			if(matches.position() != 0) {
				continue;
			}

			std::string match = matches[0];

			Token *t = tokenPattern->callback(match);

			if(t == NULL) {
				continue;
			}

			auto matchLength = match.size();

			auto sourceLookup = sourceFile->Lookup(lexedBytes);
			
			t->Tag(sourceFile->GetFileName(), sourceLookup.first, sourceLookup.second, match, matchLength);

			tree->Append(t);
			
			lexedBytes += matchLength;
			fileContentsStillPendingLexing.assign(fileContentsStillPendingLexing.substr(matchLength));
			foundToken = true;
			break;
		}
		
		if(!foundToken) {
			auto sourceLookup = sourceFile->Lookup(lexedBytes);
			std::cout << "Syntax Error: Line " << sourceLookup.first << " Character #" << sourceLookup.second << std::endl;
			break;
		}
	}
	return tree;
}

void Lexer::AddTokenPattern(std::string pattern, TokenPatternCallback callback) {
	auto tokenPattern = new TokenPattern();
	tokenPattern->pattern = pattern;
	tokenPattern->callback = callback;
	this->TokenPatterns.push_back(tokenPattern);
}