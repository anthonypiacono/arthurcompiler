#ifndef __QUOTEDSTRING_H__
#define __QUOTEDSTRING_H__

#include <string>
#include <map>
#include <vector>
#include <regex>
#include <boost/algorithm/string.hpp>

class QuotedString {
public:
	QuotedString(std::string quotedStr);

	bool IsDoubleQuoted();
	std::string Str();

private:
	bool DoubleQuoted;
	std::string String;
};

#endif