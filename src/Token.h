#ifndef __TOKEN_H__
#define __TOKEN_H__

#include <string>
#include <vector>

class Token {
public:
	Token(std::string operation, std::vector<std::string> parameters);
	Token(std::string operation);

	std::string Op();
	size_t ParamCount();
	std::string Param(size_t n);
	std::vector<std::string> Params();

	void Tag(std::string fileName, unsigned int line, unsigned int character, std::string text, unsigned int length);

	std::string GetFileName() { return FileName; }
	unsigned int GetLine() { return Line; }
	unsigned int GetCharacter() { return Character; }
	std::string GetText() { return Text; }
	unsigned int GetLength() { return Length; }

private:
	std::string Operation;
	std::vector<std::string> Parameters;

	std::string FileName;
	unsigned int Line;
	unsigned int Character;
	std::string Text;
	unsigned int Length;
};

#endif