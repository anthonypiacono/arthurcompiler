#include <iostream>
#include "Compiler.h"

int main(int argc, char **argv) {
	std::string inputFile = argc >= 2
		? argv[1]
		: "main.ars";

	std::string outputFile = argc >= 3
		? argv[2]
		:"output.ar";

	std::vector<std::string> includeDirectories;

	std::vector<std::string> errors;

	if(!g_Compiler.Compile(inputFile, outputFile, includeDirectories, &errors)) {
		std::cout << "Errors";
	}

	system("PAUSE");

	return 0;
}