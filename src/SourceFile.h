#ifndef __SOURCEFILE_H__
#define __SOURCEFILE_H__

#include <vector>
#include <string>
#include <fstream>

class SourceFile {
private:
	SourceFile(std::string fileName, std::string contents);
	std::string FileName;
	std::vector<std::string> Lines;
	std::string Contents;

public:
	~SourceFile();
	static SourceFile *Open(std::string fileName);

	std::string GetContents();
	std::string GetFileName();
	std::pair<unsigned int,unsigned int> Lookup(unsigned int index);
};

#endif