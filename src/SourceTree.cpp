#include "SourceTree.h"

SourceTree::SourceTree() {
}

SourceTree::~SourceTree() {
}

void SourceTree::Append(Token *token) {
	this->Tokens.push_back(token);
}

void SourceTree::Prepend(Token *token) {
	this->Tokens.insert(this->Tokens.begin(), token);
}

Token *SourceTree::At(size_t index) {
	if(index >= this->Size()) {
		return NULL;
	}

	return this->Tokens[index];
}

size_t SourceTree::Size() {
	return this->Tokens.size();
}

void SourceTree::Insert(Token *token, unsigned int index) {
	this->Tokens.insert(this->Tokens.begin() + index, token);
}

void SourceTree::Insert(SourceTree *tree, unsigned int index) {
	auto size = tree->Size();

	for(unsigned int x = 0; x < size; x++) {
		Insert(tree->At(x), index + x);
	}
}