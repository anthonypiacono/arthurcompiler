#pragma once

#include <string>
#include <vector>
#include <map>
#include <boost\function.hpp>
#include <boost\bind.hpp>
#include "Lexer.h"
#include "Compilation.h"

class Compiler {
public:
	Compiler();
	~Compiler();

	bool Compile(std::string inputFile, std::string outputFile, std::vector<std::string> includeDirectories, std::vector<std::string> *errors);
};

extern Compiler g_Compiler;